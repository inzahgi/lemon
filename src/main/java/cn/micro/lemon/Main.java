package cn.micro.lemon;

import cn.micro.lemon.server.LemonServer;

/**
 * Main
 *
 * @author lry
 */
public class Main {

    public static void main(String[] args) {
        new LemonServer().initialize();
    }

}
